## To verify whether the docker is present in linux, run this code

**$ sudo -v**

 ## To verify the docker system present in host, run this code

 `$ docker --version`

 `Docker version 19.03.1, build 74b1e89`

## Designing the network strategy for _*Influx DB*_

 It is important to review few details,by default newly created containers run on the bridge network stack. After installing _Influx DB_, we can bind telegraf but it does not expose any ports to the current host stack. Eventually run *Influx DB* on the default bridge and having run telegraf in the same stack.
  
### Prepare Influx DB for the Docker

`$ sudo useradd -rs /bin/false influxdb`

`$ sudo mkdir -p /etc/influxdb'// In etc directory, create a folder for Influx DB configuration`

### Creating a configuration file for Influx DB and Docker

`docker run --rm influxdb influxd config | sudo tee`

`/etc/influxdb/influxdb.conf &gt; /dev/null`

The influxd config command will print a full InfluxDB configuration file,As the –rm option is set, Docker will run a container in order to execute this command and the container will be deleted as soon as it exits.

`$ sudo chown influxdb:influxdb /etc/influxdb/*`

### Creating a Lib folder for InfluxDB and Docker
As stated in the documentation, InfluxDB stores its data, metadata as well as the WAL (for write-ahead log) in the /var/lib/influxdb folder by default.

`$ sudo mkdir -p /var/lib/influxdb` //code if the folder doesn't exist

`$ sudo chown influxdb:influxdb /var/lib/influxdb` /* to make sure the permissions are set for the container*/

### Anatomy of the InfluxDB image
On container boot, the **entrypoint.sh** script is executed, it is set as the entrypoint of your Docker container.
- To execute the entrypoint script: by executing the init-influxdb script.
  -First, it will watch for environment variables passed to your docker command, and it will execute commands accordingly.
  -Next, if you have a docker-entrypoint-initdb.d directory at the root directory of your container, it will execute either bash scripts or IQL scripts in it.

We are going to use this information to create our InfluxDB container.

First, make sure that no folders are already created in your `/var/lib/influxdb folder`.

`$ ls -l /var/lib/influxdb`
`total 0`

Execute the following command for the meta folder (in the influxdb folder) to be updated with the correct information. As a reminder, we want an admin account and a regular account for Telegraf (named telegraf).

### Creating Initialization Scripts on Your Host

In order for the initialization scripts to run on initialization, they have to be mapped to the docker-entrypoint-initdb.d folder in your container.
First,create a scripts folder on your host wherever you want.
`/etc/influxdb.`

`$ sudo mkdir -p /etc/influxdb/scripts`
Edit a new script file on your newly created folder, and make sure to give it a .iql extension:
`$ sudo touch influxdb-init.iql`

The last step will be to prepare our meta folder for InfluxDB initialization.
>Creating/Updating the InfluxDB Meta Database

`$ docker run --rm -e INFLUXDB_HTTP_AUTH_ENABLED=true \ `

         `-e INFLUXDB_ADMIN_USER=admin \ `

         `-e INFLUXDB_ADMIN_PASSWORD=admin123 \`

         `-v /var/lib/influxdb:/var/lib/influxdb \`

         `-v /etc/influxdb/scripts:/docker-entrypoint-initdb.d \`

         `influxdb /init-influxdb.sh`

Authentication is enabled in one of the next sections; this parameter is only used for the initialization script.Please make sure that a couple of logs printed to your terminal. If this is not the case, make sure that you specified the correct environment variables for your container.
As the last verification step, you can inspect your meta.db file in your meta folder to make sure that the changes were correctly written.
`$ cat /var/lib/influxdb/meta/meta.db | grep one_week`

### Verifying InfluxDB Configuration for Docker

- A simple configuration file in the /etc/influxdb folder, open and verify to see if everything is correct.
- Head over to the [http] section of your configuration and make sure that it is enabled.
-Verify that the bind-address is set to 8086 by default. This is the port that we are going to use to send some commands to InfluxDB database, like creating a database or adding a user for example.


### Data, Meta and WAL configurations

By default, the configuration file should have the paths that is created.However, once should check the paths are correct.
`[meta]`
 `dir = "/var/lib/influxdb/meta"`
 `[data]`
 `dir = "/var/lib/influxdb/data"`
 `wal-dir = "/var/lib/influxdb/wal"`

### Running the InfluxDB container on Docker
`$ sudo netstat -tulpn | grep 8086`

We configured folders to be accessible by the InfluxDB user (belonging in the InfluxDB group). As a consequence, we will need the user ID of the InfluxDB user in order to run our container.

To find the InfluxDB user ID, head over to the password file in host and run:
`$ cat /etc/passwd | grep influxdb`
`influxdb:x:997:997::/var/lib/influxdb:/bin/false`

Modify the user ID accordingly when running the docker command.

### To start InfluxDB on Docker, run the following command:


 `docker run -d -p 8086:8086 --user 997:997 --name=influxdb \ `

 `-v /etc/influxdb/influxdb.conf:/etc/influxdb/influxdb.conf \`

 `-v /var/lib/influxdb:/var/lib/influxdb \ `

 `influxdb \ `

 `-config /etc/influxdb/influxdb.conf`

### Testing Your InfluxDB Container:
In order to test if InfluxDB container is correctly running, you can check that the HTTP API is correctly enabled:
`$ curl -G http://localhost:8086/query --data-urlencode "q=SHOW DATABASES"`

Yto check if InfluxDB server is correctly listening on port 8086 in host:
`$ netstat -tulpn | grep 8086`
`tcp6    0    0 :::8086      :::*       LISTEN     `

By default, the InfluxDB server does not contain any databases except for the _internal used, as its name describes, internal metrics about InfluxDB itself. However, if you created initialization scripts for InfluxDB database, make sure that databases and retention policies are correctly assigned.

| code| 
| ------ | 
| $ influx|
| Connected to http://localhost:8086 version 1.7.8 | 
| InfluxDB shell version: 1.7.7 |
| &gt; SHOW USERS |
| user    admin |
| ----    ----- |
| admin   true |
| &gt; SHOW DATABASES |
| name: databases |
| name |
| ---- |
| weather |

### Create an Administrator Account with Docker exec:-
It is not necassary to create an administrator account if one has initialized InfluxDB image with environment variables in the previous sections.
To create an administrator account, run this code
`$ docker container ls`
` class=""&gt;$ docker container ls`

Identify the container ID of InfluxDB container, and run the following command to have a bash in the container
`$ docker exec -it &lt;container_id&gt; /bin/bash`

Here are the options specified with it:

-i : for interactive, it will keep the standard input open even if not attached
-t : to allocate a pseudo-TTY to your current shell environment.

In the container, run the influx utility to create the administrator account.

`$ influx`

`Connected to http://localhost:8086 version 1.7.8`
 
`InfluxDB shell version: 1.7.8`
 
`&gt; CREATE USER admin WITH PASSWORD 'admin123' WITH ALL PRIVILEGES`

`&gt; SHOW USERS`

`user  admin`

`----  -----`

`admin true`

### Enable HTTP Authentication in the Configuration File:

`$ sudo nano /etc/influxdb/influxdb.conf`

| code   |
| ------ |
| [http] | 
| enabled = true |
| bind-address = ":8086" |
| auth-enabled = true |

Save the file and restart the container for the changes to be applied:

`$ docker container restart`

You should be unable to execute a query without specifying the correct credentials:

`$ curl -G http://localhost:8086/query --data-urlencode "q=SHOW DATABASES"`

`{"error":"unable to parse authentication credentials"}`

Let’s try to execute the InfluxQL query again with correct credentials:

`$ curl -G -u admin:admin123 http://localhost:8086/query --data-urlencode "q=SHOW DATABASES"`

`{"results":[{"statement_id":0,"series":[{"name":"databases","columns":["name"],"values":[["_internal"]]}]}]}`

With this curl command, we made sure that the credentials were correctly set up for our InfluxDB server.
It is time to install metrics collection agent: Telegraf.

## Telegraf
 
 It is a plugin-driven agent that periodically collects metrics from a variety of different systems. The metrics are pushed to InfluxDB and they can be later analyzed in Chronograf or Grafana. Luckily, Telegraf also belongs to the official Docker images.

### Prepare Telegraf for InfluxDB and Docker

Similarly to our InfluxDB setup, one should create a Telegraf user for the host. It ensures that correct permissions are set for the future configuration files.

`$ sudo useradd -rs /bin/false telegraf`

In the etc directory, create a new folder for the Telegraf configuration files.

`$ sudo mkdir -p /etc/telegraf `

### CREATING A CONFIGURATION FILE FOR TELEGRAF AND DOCKER

The Telegraf Docker image is built very closely to the InfluxDB one.

As a consequence, it is able to run a simple telegraf config command to generate a configuration on the fly.

The Telegraf configuration file has the following defaults:
- Interval: 10 seconds. Telegraf is going to gather and send metrics to InfluxDB every 10 seconds.
- Round_interval : true. The agent is going to collect metrics on :00, :10, or :(00 + n*interval)
- The InfluxDB output plugin is enabled by default.
- The CPU, disk, diskio, kernel, memory, processes, swap and system inputs plugins are enabled. As those inputs use the /proc mountpoint to gather metrics, we will have to remap volumes on the container.

To create a Telegraf configuration file using Docker, run the following command.

`docker run --rm telegraf telegraf config | sudo tee /etc/telegraf/telegraf.conf &lt; /dev/null`

Reassign the correct permissions to the Telegraf configuration folder.

`$ sudo chown telegraf:telegraf /etc/telegraf/*`

### Modify the Telegraf Configuration File

By default, Telegraf will send metrics to a database named “telegraf” on InfluxDB. This is a customizable parameter; however, in this case, we are only going to specify the InfluxDB authentication parameters.
Edit the Telegraf configuration file, and locate the [[outputs.influxdb]] section.

>In this configuration file, locate the “HTTP Basic Auth” section and modify the credentials accordingly.

`## HTTP Basic Auth`

`username = "admin"`

`password = "admin123"`

save and exit. run the conatainer.

#### Running the Telegraf Container on Docker

Telegraf enables system inputs by default. As a consequence, we have to remap the /proc host folder to the /host folder in the Docker image.
This is to ensure that Telegraf is not gathering metrics from the Docker container itself, and that the container filesystem is not altered in any way.
To achieve this, unless you gave a name to the InfluxDB container, run this command to get the InfluxDB container ID. It will be used to connect Telegraf and InfluxDB to the same virtual network.

`$ docker container ls | grep influxdb`

`1939ba611410   influxdb   "/entrypoint.sh -conf..."   24 minutes ago    Up 30 minutes    0.0.0.0:8086-&lt;8086/tcp    ecstatic_moore`

Isolate the Telegraf user ID by running the following command:

`$ getent passwd | grep telegraf`

`telegraf:x:998:998::/etc/telegraf:/bin/false`

Next, to run the Telegraf Docker image, run the following command:

$ docker run -d --user 998:998 --name=telegraf \
      --net=container: \
      -e HOST_PROC=/host/proc \
      -v /proc:/host/proc:ro \
      -v /etc/telegraf/telegraf.conf:/etc/telegraf/telegraf.conf:ro \
      telegraf

**Note:** the net option can be replaced by –net=influxdb if you chose to create your InfluxDB container with a name.

To make sure the Telegraf instance is running correctly, run the following command:
	
`$ docker container logs -f --since 10m telegraf`

>  To double-check the correctness of the setup by inspecting the InfluxDB database:

`$ docker exec -it  influx -username admin -password admin123`

`InfluxDB shell version 1.7.8`

`&lt; SHOW DATABASES`

|code|
|----|
|`name: databases`|
|`name`|
|`----`|
|`weather`|
|`_internal`|
|`telegraf`|

`&lt; USE telegraf`
`&lt; SELECT * FROM cpu WHERE time &gt; now() - 1m`

### Visualizing Telegraf Metrics in Grafana

It is a dashboarding tool that pulls from a wide variety of different datasources in order to create beautiful graphs and dashboards. It can pull from traditional SQL databases, but it can also pull from a variety of time series databases.

### Installing Grafana on Docker

Start the container binding the external port 3000.

`docker run -d --name=grafana -p 3000:3000 grafana/grafana`
 
`grafana/grafana:<version>`

`grafana/grafana:<version>-ubuntu`
This image is based on Ubuntu, available in the Ubuntu official image. This is an alternative image for those who prefer an Ubuntu based image and/or who are dependent on certain tooling not available for Alpine.

Run the latest stable version of Grafana
`docker run -d -p 3000:3000 grafana/grafana`

### Run the Grafana master branch

For every successful build of the master branch, we update the `grafana/grafana:master` and` grafana/grafana:master-ubuntu tags`. Additionally, two new tags are created, `grafana/grafana-dev:master-<commit hash>` and `grafana/grafana-dev:master-<commit hash>-ubuntu`, which includes the hash of the Git commit that was built. Use these to get access to the latest master builds of Grafana.

When running Grafana master in production, the `grafana/grafana-dev:master-<commit hash> tag`. This tag guarantees that to use a specific version of Grafana instead of whatever was the most recent commit at the time.

Pass the plugins to install it to Docker with the `GF_INSTALL_PLUGINS` environment variable as a comma-separated list. This sends each plugin name to `grafana-cli plugins install ${plugin}` and installs them when Grafana starts.

`docker run -d \
 -p 3000:3000 \
 --name=grafana \
 -e "GF_INSTALL_PLUGINS=grafana-clock-panel,grafana-simple-json-datasource" \
 grafana/grafana`

#### To create a Grafana container, run the following command in the host:

`$ docker run -d --name=grafana -p 3000:3000 grafana/grafana`

** Grafana server container should now be up and running in the host. To confirm that, run the following command.**

`$ docker container ls | grep grafana`

**To make sure that it is correctly listening on port 3000.**

`$ netstat -tulpn | grep 3000`

### Configuring Grafana for InfluxDB

In the  web browser, head over to http://localhost:3000

It redirected to Grafana homepage. The default credentials for Grafana are admin/admin. Immediately, you are asked to change your password. Choose a strong password and click on “Save.”

- It should now be redirected to the Grafana default Web UI:
- Click on “Add data source” to add an InfluxDB datasource:
- Next, select the InfluxDB option and click on “Select.”

run the following command:


`$ docker network inspect bridge | grep influxdb -A 5`

`"Name": "influxdb",`

`"EndpointID": "7e4eb0574a346687efbb96b6b45",`

`"MacAddress": "02:42:ac:11:00:04",`

`"IPv4Address": "172.17.0.2/16",`

`"IPv6Address": ""`

- Copy the IPv4 address and paste it in the InfluxDB configuration for Grafana

fill in the necassary details required.
- Click on “Save and Test” to make sure that your configuration is working properly.


